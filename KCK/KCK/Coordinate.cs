﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KCK
{
    public class Coordinate
    {
        public int X { get; set; }
        public int Y { get; set; }
        public char znak;
        public static List<Coordinate> listOfElements = new List<Coordinate>();
        public Coordinate()
        {
            X = 0;
            Y = 0;
        }
        public Coordinate(int x, int y)
        {
            this.X = x;
            this.Y = y;
        }
        public Coordinate(int x, int y,char z)
        {
            this.X = x;
            this.Y = y;
            this.znak = z;
        }
        //public static Boolean checkElement (int x, int y)
        //{
        //    foreach (Coordinate elem in listOfElements)
        //    {
        //        if (elem.X == x && elem.Y==y)
        //            return true;
        //    }
        //    return false;

        //}
        //public static void usunElement (int x, int y)
        //{
        //    foreach (Coordinate elem in listOfElements)
        //    {
        //        if (elem.X == x && elem.Y == y)
        //            listOfElements.Remove(elem);
        //    }
        //}
        public int getX()
        {
            return this.X;

        }
        public int getY()
        {
            return this.Y;
        }
    }
    
}
