﻿using System;
using System.Linq;
using System.Text;
using System.IO;
using System.Collections.Generic;
using System.Threading;
using System.Windows.Input;


namespace KCK
{
    class Program
    {
        public const int initialWindowHeight = 40;
        public const int initialWindowWidth = 110;
        public static ConsoleKeyInfo key;
       

        static void Main(string[] args)
        {
            string []snejk ={".----------------.  .-----------------. .----------------.  .----------------.  .----------------.  ",
                          "| .--------------. || .--------------. || .--------------. || .--------------. || .--------------. |",
                          "| |    _______   | || | ____  _____  | || |  _________   | || |     _____    | || |  ___  ____   | |",
                          "| |   /  ___  |  | || ||_   \\|_   _| | || | |_   ___  |  | || |    |_   _|   | || | |_  ||_  _|  | |",
                          "| |  |  (__ \\_|  | || |  |   \\ | |   | || |   | |_  \\_|  | || |      | |     | || |   | |_/ /    | |",
                          "| |   '.___`-.   | || |  | |\\ \\| |   | || |   |  _|  _   | || |   _  | |     | || |   |  __'.    | |",
                          "| |  |`\\____) |  | || | _| |_\\   |_  | || |  _| |___/ |  | || |  | |_' |     | || |  _| |  \\ \\_  | |",
                          "| |  |_______.'  | || ||_____|\\____| | || | |_________|  | || |  `.___.'     | || | |____||____| | |",
                          "| |              | || |              | || |              | || |              | || |              | |",
                          "| '--------------' || '--------------' || '--------------' || '--------------' || '--------------' |",
                          " '----------------'  '----------------'  '----------------'  '----------------'  '----------------' ",
                          "" };
            Console.SetWindowSize(initialWindowWidth, initialWindowHeight);
            Console.SetBufferSize(Console.WindowWidth, Console.WindowHeight);
            
            for (int i=0;i<snejk.Length;i++)
            {
                Console.BackgroundColor = ConsoleColor.Black;
                Console.Write(new string(' ', 5));
                Console.BackgroundColor = ConsoleColor.Gray;
                Console.ForegroundColor = ConsoleColor.DarkRed;
                Console.WriteLine(snejk[i]);//rozpoczęcie programu
            }
            Console.CursorVisible = false;
            Console.BackgroundColor = ConsoleColor.Black;
            Console.WriteLine("Press any key to play...");
            Console.ReadKey();//przejscie do menu            
            Menu();//menu            
            Environment.Exit(0);           
        }
        static void Menu()
        {
            
            List<String> menuList = new List<String>()
            {
                "S T A R T\n\n","W Y B I E R Z   P O Z I O M\n\n","E X I T\n\n"
            };
            
            Console.Clear();
            bool choice = true;
            int level=0;
            int cursor=0;           
            int origRow = Console.CursorTop, origCol = Console.CursorLeft;
            Console.CursorVisible = false;
            
            while (choice)//nieskończona pętla menu w której wybieramy opcje
            {
                Console.SetBufferSize(Console.WindowWidth, Console.WindowHeight);
                Console.SetCursorPosition(0,10);
                Console.BackgroundColor = ConsoleColor.Black;
                for (int i= 0; i<menuList.Count;i++)
                {
                    Console.Write(new string(' ', (Console.WindowWidth - menuList[i].Length+4) / 2));                 
                    if (i==cursor)
                    {
                        Console.BackgroundColor = ConsoleColor.Red;
                        Console.ForegroundColor = ConsoleColor.Black;                       
                        Console.WriteLine(menuList[i]);
                    }
                    else
                    {                       
                        Console.WriteLine(menuList[i]);
                    }                   
                    Console.ResetColor();
                }
                
                key = Console.ReadKey();
                switch (key.Key)
                {
                    case ConsoleKey.UpArrow:
                        if (cursor <= 0)
                        {
                            //cursor = menuList.Count-1;
                        }
                        else cursor--;
                        break;
                    case ConsoleKey.DownArrow:
                        if (cursor == menuList.Count - 1)
                        { 
                            //cursor = 0;
                        }
                        else cursor++;
                        break;
                    case ConsoleKey.Enter:
                        if (cursor == 0) { startGame(level);
                            Console.BackgroundColor = ConsoleColor.Black;
                           
                        }
                        else if (cursor == 1) level = chooseLevel();//wybór poziomu trudności???
                        else if (cursor == 2)//przejście do wyjścia 
                        {
                            choice = Exit(choice);
                        }
                        break;
                }
                Console.Clear();
                
            }
        }
        private static void WriteCharacterStrings(int start, int end,
                                             bool changeColor)
        {
            for (int ctr = start; ctr <= end; ctr++)
            {
                if (changeColor)
                    Console.BackgroundColor = ConsoleColor.Blue;

                Console.WriteLine(new String(Convert.ToChar(ctr + 64), 30));
            }
        }
        static bool Exit(bool exit)//upewnienie się że chcemy wyjść
        {
            bool choice = true;
            int cursor=0;
            List<String> menuList = new List<String>()
            {
                "Are you sure you want to exit?","Tak","Nie"
            };
            Console.Clear();
            ConsoleKeyInfo key;                   
            cursor++;
            while (choice)
            {
                Console.SetCursorPosition(0, 10);
                Console.BackgroundColor = ConsoleColor.Black;
                for (int i = 0; i < menuList.Count; i++)
                {
                    Console.Write(new string(' ', (Console.WindowWidth - menuList[i].Length ) / 2));
                    if (i == cursor)
                    {
                        Console.BackgroundColor = ConsoleColor.Red;
                        Console.ForegroundColor = ConsoleColor.Black;
                        Console.WriteLine(menuList[i]);
                    }
                    else
                    {
                        Console.WriteLine(menuList[i]);
                    }
                    Console.ResetColor();
                }
                key = Console.ReadKey();
                switch (key.Key)
                {
                    case ConsoleKey.UpArrow:
                        if (cursor <= 1)
                        {
                            //cursor = menuList.Count-1;
                        }
                        else cursor--;
                        break;

                    case ConsoleKey.DownArrow:
                        if (cursor == menuList.Count-1)
                        {
                            //cursor = 0;
                        }
                        else cursor++;
                        break;

                    case ConsoleKey.Enter:
                        if (cursor==1) {
                            Console.Clear();
                            
                            exit = false;
                        }
                        if (cursor==2) {
                            Console.Clear();
                            exit = true;
                                };
                        choice = false;
                        break;
                }
                Console.Clear();

            }              
                return exit;
            }
        static void startGame(int level)
        {
            Game game = new Game(level);
            game.gamePlay();
            game.youDied();
        
        }
        
        static int chooseLevel()
        {
            int level=0;
            List<String> menuList = new List<String>()
            {
                "Beginner","Semi-Advanced","Advanced","Pro","Wstecz"
            };

            Console.Clear();
            bool choice = true;

            int cursor = 0;
            int origRow = Console.CursorTop, origCol = Console.CursorLeft;
            Console.CursorVisible = false;
            while (choice)//nieskończona pętla menu w której wybieramy opcje
            {
                Console.SetCursorPosition(0, 10);
                Console.BackgroundColor = ConsoleColor.Black;
                for (int i = 0; i < menuList.Count; i++)
                {
                    Console.Write(new string(' ', (Console.WindowWidth - menuList[i].Length ) / 2));
                    if (i == cursor)
                    {
                        Console.BackgroundColor = ConsoleColor.Red;
                        Console.ForegroundColor = ConsoleColor.Black;
                        Console.WriteLine(menuList[i]);
                    }
                    else
                    {
                        Console.WriteLine(menuList[i]);
                    }
                    Console.ResetColor();
                }

                key = Console.ReadKey();
                switch (key.Key)
                {
                    case ConsoleKey.UpArrow:
                        if (cursor <= 0)
                        {
                            //cursor = menuList.Count-1;
                        }
                        else cursor--;
                        break;
                    case ConsoleKey.DownArrow:
                        if (cursor == menuList.Count - 1)
                        {
                            //cursor = 0;
                        }
                        else cursor++;
                        break;
                    case ConsoleKey.Enter:
                        if (cursor == menuList.Count - 1) { choice = false; }
                        else
                        {
                            level = cursor;
                            choice = false;
                        }
                        break;
                    case ConsoleKey.Escape:
                        choice = false;
                        break;


                }
                Console.Clear();
                

            }
            return level;
        }
    }

        
    }
