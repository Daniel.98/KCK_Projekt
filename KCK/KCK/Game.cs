﻿using System;
using System.Linq;
using System.Text;
using System.IO;
using System.Collections.Generic;
using System.Threading;
using System.Windows.Input;

namespace KCK
{
    public class Game
    {
        public ConsoleKeyInfo key;
        private int rows;
        private int columns;
        private Snake snake;
        private Food food;
        private Coordinate foodPosition;
        private char[,] pole;
        private int speed;
        private int level;
        private Obstacle obstacle;
        public Game(int level)
        {
            this.level = level;
            chooseLevel(level);
            food = new Food(rows, columns);
            snake = new Snake();
            obstacle = new Obstacle(rows,columns);
            initPole(rows,columns);
            initConsole();
            writePole();
        }
        
        
        public int getrows()
        {
            return rows;
        }
        public int getcolums()
        {
            return columns;
        }
        public void initPole(int rows, int columns)
        {
            //potrzeba ustawiania wielkości pola w zależności od levelu
            pole = new char[rows, columns];
            for (int i = 0; i < columns; i++)
            {
                pole[0, i] = 'x';
                pole[rows - 1, i] = 'x';

            }
            for (int i = 0; i < rows; i++)
            {
                pole[i, 0] = 'x';
                pole[i, columns - 1] = 'x';

            }
        }
        public void initConsole()
        {
           
            Console.SetWindowSize(columns+10, rows+10);
            Console.SetBufferSize(Console.WindowWidth,Console.WindowHeight);
            Console.BackgroundColor = ConsoleColor.DarkBlue;
            Console.Clear();

        }
        public void writePole()
        {
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < columns; j++)
                    Console.Write(pole[i, j]);
                Console.WriteLine();
            }
            foodPosition=  food.DrawFood(snake.Body, Obstacle.listObstacle);

        }
        /// <summary>
        /// Sprawdzenie czy wąż koliduje z obramowaniem pola
        /// </summary>
        /// <returns></returns>
        public bool poleCollision()
        {
            Coordinate temp = snake.GetCoordinate();
            if (temp.getX() == columns-1 || temp.getY() == rows-1 || temp.getY() == 0 || temp.getX() == 0) return true;
            else if (obstacle.check(temp) == true)
            {
                return true;
            }
            
            return false;
        }

        
        public void youDied()
        {
            Console.Clear();
            Console.SetCursorPosition(0, 0);
            Console.WriteLine("you lost");
            Console.WriteLine("your score:" + (snake.length-3));
            Obstacle.listObstacle = new List<Coordinate>();
            Console.ReadKey();
            Console.SetWindowSize(110, 40);
        }
        public void gamePlay()
        {
            int x = 100;
               
           // Direction dir=Direction.Prawo;
            int pom1 = 0, pom2 = 0;
            int ograniczenie = 5;
            
            for (; ; )
            {
                if (key.Key == ConsoleKey.UpArrow || key.Key == ConsoleKey.DownArrow) Thread.Sleep(x);
                if (key.Key == ConsoleKey.LeftArrow || key.Key == ConsoleKey.RightArrow) Thread.Sleep(x);
                else Thread.Sleep(x);
                if (Console.KeyAvailable == true)
                    //dir = snake.getKierunek();
                    key = Console.ReadKey();
                switch (key.Key)
                {
                    case ConsoleKey.UpArrow:
                        if (snake.Kierunek== Direction.Dol) break;
                        snake.Kierunek = Direction.Gora;
                        break;

                    case ConsoleKey.DownArrow:
                        if (snake.Kierunek== Direction.Gora) break;
                        snake.Kierunek = Direction.Dol;
                        break;

                    case ConsoleKey.RightArrow:
                        if (snake.Kierunek == Direction.Lewo) break;
                        snake.Kierunek = Direction.Prawo;
                        break;
                    case ConsoleKey.LeftArrow:
                        if (snake.Kierunek== Direction.Prawo) break;
                        snake.Kierunek = Direction.Lewo;
                        break;
                    default:
                        break;

                }
                if(snake.Move(snake))break;

                
                if (foodPosition.X == snake.HeadPosition.X
                    && foodPosition.Y == snake.HeadPosition.Y)
                {
                    pom1++;
                    
                    snake.EatFood();
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    foodPosition = food.DrawFood(snake.Body, Obstacle.listObstacle);
                }
                if (poleCollision()) break;
                if (pom1 != pom2 && pom1 % 2 == 0)
                {
                    pom2++;
                    x = x - 5;
                    Console.ForegroundColor = ConsoleColor.Red;
                    obstacle.DrawObstacle(snake.Body, snake.HeadPosition, foodPosition);
                }

                

            }


        }
        public void chooseLevel(int level)
        {
            this.level = level;
            string line;          
            int counter=0;
            string textFile= @"C:\Users\User\Documents\GitHub\KCK_Projekt\KCK\KCK\Levels.txt";
            using (StreamReader file = new StreamReader(textFile))
            {
                while((line=file.ReadLine())!=null)
                {
                    if (counter == level) break;
                    counter++;
                }
                string[] parts = line.Split(new[] { ',' });
                rows = Convert.ToInt32(parts[0]);
                columns = Convert.ToInt32(parts[1]);
                file.Close();
            }
            
        }
    }
}
