﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KCK
{
    public class Obstacle
    {
        public int x;
        public int y;
        private int rows;
        private int columns;
        public static Random rand = new Random();
        public static List<Coordinate> listObstacle=new List<Coordinate>();
        public Obstacle(int rows,int columns)
        {
            this.columns = columns;
            this.rows = rows;
            
        }

        
          
        public static List<Coordinate> getListOfObstacles()
        {
            return listObstacle;
        }
        public Coordinate PositionObstacle { get; set; }
        public void DrawObstacle(List<Coordinate> list, Coordinate head, Coordinate food)
        {
           
            Boolean kolizja = false;
            do {
                kolizja = false;
                x = rand.Next(2, columns-2);
                y = rand.Next(2, rows-2);
                
                foreach (Coordinate elem in listObstacle)
                {
                    if (elem.X == x && elem.Y == y)
                        kolizja = true;
                }

                foreach (Coordinate elem in list)
                {
                    if (elem.X == x && elem.Y == y)
                        kolizja = true;
                }
                if (x == food.X && y == food.Y)
                    kolizja = true;

                if (Math.Abs(head.X - x) < 6 && Math.Abs(head.Y - y) < 6)
                    kolizja = true;

            } while (kolizja);

            //do
            //{
            //    kolizja = false;
                

                
            //    if( kolizja == true)
            //    {
            //        x = rand.Next(2, 15);
            //        y = rand.Next(2, 15);
            //    }

            //} while (kolizja);                       
            PositionObstacle = new Coordinate(x++, y, '[');
            Coordinate.listOfElements.Add(PositionObstacle);
            listObstacle.Add(PositionObstacle);
            PositionObstacle = new Coordinate(x, y, ']');
            Coordinate.listOfElements.Add(PositionObstacle);
            listObstacle.Add(PositionObstacle);

            foreach (Coordinate elem in listObstacle)
            {
                Console.SetCursorPosition(elem.X, elem.Y);
                Console.Write(elem.znak);
            }

        }
        
        public bool check(Coordinate temp)
        {
            foreach(Coordinate elem in listObstacle)
            {
                if(temp.getX()==elem.X && temp.getY() == elem.Y)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
