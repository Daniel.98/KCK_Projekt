﻿using System.Text;
using System.IO;
using System.Collections.Generic;
using System.Threading;
using System.Windows.Input;
using System;
using System.Linq;

namespace KCK
{
    public class Food
    {
        public int x = 2;
        public int y = 8;
        private int columns = 0;
        private int rows = 0;
        public static Random rand = new Random();
        public Food(int rows, int columns)
        {

            this.columns = columns;
            this.rows = rows;

        }
        public  Coordinate PositionFood { get; set; }
        public Coordinate DrawFood(List<Coordinate> listWaz, List<Coordinate> obstacles)
        {

            Boolean kolizja = false;
            do
            {
                kolizja = false;
                x = rand.Next(2, columns-1);
                y = rand.Next(2, rows-1);

                foreach (Coordinate elem in listWaz)
                {
                    if (elem.X == x && elem.Y == y)
                    {
                        kolizja = true;
                        break;
                    }
                }
                foreach (Coordinate elem in obstacles)
                {
                    if (elem.X == x && elem.Y == y)
                    {
                        kolizja = true;
                        break;
                    }
                }

            } while (kolizja);

            //do
            //{
            //    kolizja = false;



            //    if (kolizja == true)
            //    {
            //        x = rand.Next(2, 15);
            //        y = rand.Next(2, 15);
            //    }

            //} while (kolizja);
            //x = rand.Next(2, 15);
            //y = rand.Next(2, 15);
            PositionFood = new Coordinate(x, y);
            //Coordinate.listOfElements.Add(PositionFood);
            Console.SetCursorPosition(PositionFood.X, PositionFood.Y);
            Console.Write("$");
            return PositionFood;
        }

    }
}
