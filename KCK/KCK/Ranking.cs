﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KCK
{
    public interface IRanking
    {
        public Wynik GetRanking(int id);
        public void RemoveRanking(int id);
        
    }
    public class Wynik
    {
        private int wynik { get; set; }
        private string nazwa { get; set; }
        public Wynik(int wynik,string nazwa)
        {
            this.wynik = wynik;
            this.nazwa = nazwa;
        }
    }
    public class Ranking : IRanking
    {

       
        private List<Wynik> ranking = new List<Wynik>();
        public Ranking(string plik)
        {

        }
        public void AddRanking(int wynik,string nazwa)
        {

        }
        public Wynik GetRanking(int id)
        {
            return ranking[id];
        }
        public void RemoveRanking(int id)
        {
            ranking.RemoveAt(id);
        }



    }
}
