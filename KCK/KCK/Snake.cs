﻿using System.Text;
using System.IO;
using System.Collections.Generic;
using System.Threading;
using System.Windows.Input;
using System;
using System.Linq;

namespace KCK
{

    public interface ISnake
    {
        Boolean Move(Snake snake);
        void EatFood();
    }
    public class Snake : ISnake
    {
        public int length { get; set; } = 3;
        public Direction Kierunek { get; set; } = Direction.Prawo;
        public Coordinate HeadPosition { get; set; } = new Coordinate(3, 3);
        public List<Coordinate> Body  = new List<Coordinate>();
        public List<Coordinate> listaWeza()
        {
            return Body;
        }




        public Coordinate GetCoordinate()
        {
            return HeadPosition;
        }
        public void EatFood()
        {
            length++;
           // Coordinate.usunElement(HeadPosition.X, HeadPosition.Y);
        }
        public Direction getKierunek()
        {
            return this.Kierunek;
        }
        public Boolean Move(Snake snake)
        {
            Boolean wynik = false;
            switch (Kierunek)
            {
                case Direction.Prawo:
                    HeadPosition.X++;
                    if (snakeCollision(snake) == true)
                        wynik = true;
                        break;
                case Direction.Lewo:
                    HeadPosition.X--;
                    if (snakeCollision(snake) == true)
                        wynik = true;
                    break;
                case Direction.Gora:
                    HeadPosition.Y--;
                    if (snakeCollision(snake) == true)
                        wynik = true;
                    break;
                case Direction.Dol:

                    HeadPosition.Y++;
                    if (snakeCollision(snake) == true)
                        wynik = true;
                    break;

                default:
                    break;
                    
            }
            Console.SetCursorPosition(HeadPosition.X, HeadPosition.Y);
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("#");
           // Console.ForegroundColor = ConsoleColor.White;
            Coordinate x = new Coordinate(HeadPosition.X, HeadPosition.Y);
            Body.Add(x);
            //Coordinate.listOfElements.Add(x);
            if (Body.Count > this.length)
            {
                var endBody = Body.First();
                Console.SetCursorPosition(endBody.X, endBody.Y);
                Console.Write(" ");
               // Coordinate.usunElement(endBody.X, endBody.Y);
                Body.Remove(endBody);
                
            }
            return wynik;
        }
        //public bool snakeCollision()
        //{
        //    return true;
        //    //return false;
        //}
        public bool snakeCollision(Snake snake)
        {
            Coordinate temp = snake.GetCoordinate();
            for (int j = 0; j < snake.Body.Count(); j++)
            {

                if (temp.getX() == snake.Body[j].X && temp.getY() == snake.Body[j].Y)
                    return true;

            }
            return false;
        }


    }
    public enum Direction { Prawo, Lewo, Dol, Gora }
}
